"""
Read in data.
"""

import pandas as pd
import numpy as np
#import sklearn.
import os
import zipfile
from io import StringIO

#import matplotlib.pyplot as plt



class LifestyleDataset(object):
    def __init__(self, data_zipfile:str):
        self.data_zipfile = data_zipfile
        self.raw_data = {}
        self.data = None
        self.descriptions = None
        self.read_data()
        self.question_category()

    def read_data(self):
        with zipfile.ZipFile(self.data_zipfile, 'r') as zip_ref:
            for f in zip_ref.namelist():
                bytes_data = zip_ref.read(f)
                s=str(bytes_data,'utf-8')
                data = StringIO(s)
                df=pd.read_csv(data)
                df_name = os.path.splitext(f)[0]
                self.raw_data[df_name] = df

        self.descriptions = self.raw_data['columns']
        self.data = self.raw_data['responses']

    def question_category(self):
        """
        Get index for variables within a certain category.
        """
        #TODO
        categories = {}
        categories["music genre"] = self.data.columns[range(2,19)].tolist()
        self.categories = categories
        return categories

